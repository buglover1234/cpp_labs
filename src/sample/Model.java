package sample;

import java.util.concurrent.ExecutionException;

public class Model {
    public static String inversion(String inputStr){

        if(inputStr.isEmpty())
            return "Введите строку!!!";

        char[] charArray = inputStr.toCharArray();
        String result = "";
        for(int i = charArray.length - 1; i >= 0; i--){
            result += charArray[i];
        }

        return result;
    }
}
