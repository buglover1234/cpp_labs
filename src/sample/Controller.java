package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class Controller {

    @FXML
    private TextField input;

    @FXML
    private Text output;

    @FXML
    private void click(ActionEvent event){
        output.setText(Model.inversion(input.getText()));
    }

}
